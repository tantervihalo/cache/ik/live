import {initializeApp} from 'firebase-admin/app';
import {getFirestore} from 'firebase-admin/firestore';
import {getDatabase} from 'firebase-admin/database';
import yargs from 'yargs';
import {hideBin} from "yargs/helpers";
import {readFileSync} from 'fs';
import {resolve, dirname} from 'path';

const argv = yargs(hideBin(process.argv))
    .option('supergroup', {type: 'string', demandOption: true, desc: "Supergroup ID"})
    .option('index', {type: 'string', demandOption: true, desc: "index.json file"})
    .parse();

const {index: indexFile, supergroup: supergroupId} = argv;

const data = JSON.parse(readFileSync(indexFile, 'utf-8'));
data.groups = data.jsons.map(({file}) => JSON.parse(readFileSync(resolve(dirname(indexFile), file), 'utf-8')))
delete data.jsons;

initializeApp();
console.log("Updating Firestore...")
await getFirestore().doc(`/supergroups/${supergroupId}`).update({groupsData: JSON.stringify(data.groups)})
console.log("Updating Firebase Database...")
await getDatabase().ref(`/supergroups/${supergroupId}/data`).set(data)
console.log("Push successful")
process.exit(0)
